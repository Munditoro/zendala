# zendala
## Description

Prueba Técnica - Zendala Backend Developer.

API GraphQL construida con los framework: NestJS, Express, Apollo Server, Passport y el ORM Mongoose.

Se puede probar el servicio desplegado en la siguiente url: https://zendala-back-test.herokuapp.com/graphql

## Installation

```bash

$ yarn install

```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
 
```

## Docker

```bash

# build
$ docker build -t zendala .

# run
$ docker run -it zendala --env-file ./.env

```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Playground

```bash

# local
http://localhost:3000/graphql

# deployed
https://zendala-back-test.herokuapp.com/graphql

```

## Stay in touch

- Author - [Rubén O. Alvarado](https://www.linkedin.com/in/ruben-alvarado-molina-9020010/)
- Github - [RubenOAlvarado](https://github.com/RubenOAlvarado)
- Gitlab - [Munditoro](https://gitlab.com/Munditoro)