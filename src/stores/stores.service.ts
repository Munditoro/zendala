import { Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CustomersService } from 'src/customers/customers.service';
import { OpenpayService } from 'src/openpay/openpay.service';
import { CreateChargeInput } from './dto/create-charge.input';
import { LookStoresArgs } from './dto/look-stores.args';
import { Charge, ChargeDocument } from './models/charge.model';

@Injectable()
export class StoresService {
  constructor(
    @InjectModel(Charge.name) private readonly chargeModel: Model<ChargeDocument>,
    private openPayService: OpenpayService,
    private customerService: CustomersService
  ){}

  private readonly logger = new Logger(StoresService.name);
  
  async createChargeInStore(createChargeInput: CreateChargeInput):Promise<any> {
    try {
      const { method, amount, description } = createChargeInput;
      const {order_id, customerId } = await this.updateCharges(createChargeInput);
      const opCharge = await this.openPayService.createCharge(customerId, {
        method, amount, description, order_id
      });
      return {_id:order_id, ...opCharge};
    } catch (e) {
      this.logger.error(`Error creating new charge: ${e}`);
      throw new InternalServerErrorException('Error creating new charge');
    }
  }

  private async updateCharges(createChargeInput: CreateChargeInput):Promise<any>{
    try {
      const { customer } = createChargeInput;
      const bdCustomer = await this.customerService.findOne(customer);
      if(!bdCustomer) throw new NotFoundException('Customer not found');
      let currentCharges = bdCustomer.charges;
      let newCharge = await this.saveChargeInBd(createChargeInput);
      currentCharges = [...currentCharges, newCharge._id];
      await this.customerService.update(customer, {charges:currentCharges});
      return {
        order_id:newCharge._id.toString(),
        customerId: bdCustomer.openPayId
      };
    } catch (e) {
      this.logger.error(`Error updating customer charges in db: ${e}`);
      throw new InternalServerErrorException('Error updating customer charges in db');
    }
  }

  private async saveChargeInBd({ method, amount, description }:CreateChargeInput):Promise<Charge>{
    try {
      const newCharge = new this.chargeModel({ method, amount, description });
      return newCharge.save();
    } catch (e) {
      this.logger.error(`Error saving charge in db: ${e}`);
      throw new InternalServerErrorException('Error saving charge in db');
    }
  }

  async findAll({lon, lat, radio, amount}:LookStoresArgs):Promise<any> {
    try {
      this.logger.debug('Looking stores');
      const stores = await this.openPayService.getStores(lon, lat, radio, amount);

      if(!stores) throw new NotFoundException('No stores found');

      return stores;
    } catch (e) {
      this.logger.error(`Error looking stores: ${e}`);

      if(e instanceof NotFoundException) throw e;

      throw new InternalServerErrorException('Error looking stores');
    }
  }

}
