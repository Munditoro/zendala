import { Module } from '@nestjs/common';
import { StoresService } from './stores.service';
import { StoresResolver } from './stores.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { Charge, ChargeSchema } from './models/charge.model';
import { OpenpayModule } from 'src/openpay/openpay.module';
import { CustomersModule } from 'src/customers/customers.module';

@Module({
  imports: [
    MongooseModule.forFeature([{
      name: Charge.name,
      schema: ChargeSchema
    }]),
    OpenpayModule,
    CustomersModule,
  ],
  providers: [StoresResolver, StoresService],
  exports:[StoresService]
})
export class StoresModule {}
