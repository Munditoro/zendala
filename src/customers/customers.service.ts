import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { OpenpayService } from 'src/openpay/openpay.service';
import { CreateCustomerInput } from './dto/create-customer.input';
import { UpdateCustomerInput } from './dto/update-customer.input';
import { Customer, CustomerDocument } from './models/customer.model';

@Injectable()
export class CustomersService {
  constructor(
    @InjectModel(Customer.name) private customerModel:Model<CustomerDocument>,
    private openPayService: OpenpayService
  ){}

  private readonly logger = new Logger(CustomersService.name);

  async create({ name, last_name, email, requires_account, phone_number, address }: CreateCustomerInput):Promise<Customer>{
    try {
      this.logger.debug('Saving customer in db');

      const opCustomer = await this.openPayService.createCustomer({name, last_name, email, requires_account, phone_number, address});
      const newCustomer = new this.customerModel({openPayId: opCustomer.id, name, last_name, email, requires_account, phone_number, address });

      return newCustomer.save();
    } catch (e) {
      this.logger.error(`Error creating new customer: ${e}`);
      throw new InternalServerErrorException('Error creating new customer');
    }
  }

  async findAll():Promise<Array<Customer>>{
    try {
      this.logger.debug('Looking customers in DB');
      return await this.customerModel.find().exec();
    } catch (e) {
      this.logger.error(`Error looking customers: ${e}`);
      throw new InternalServerErrorException('Error looking customers');
    }
  }

  async findOne(id: string):Promise<Customer> {
    try {
      this.logger.debug('Looking customer by his id');
      return await this.customerModel.findById(id).exec();
    } catch (e) {
      this.logger.error(`Error looking customer by his id: ${e}`);
      throw new InternalServerErrorException('Error looking customer by his id');
    }
  }

  async update(id: string, updateCustomerInput: UpdateCustomerInput):Promise<Customer> {
    try {
      this.logger.debug('Updating customer');
      const updatedCustomer = await this.customerModel.findByIdAndUpdate(id, updateCustomerInput);
      await this.openPayService.updateCustomer(updatedCustomer.openPayId, updatedCustomer);
      return updatedCustomer;
    } catch (e) {
      this.logger.error(`Error updating customer: ${e}`);
      throw new InternalServerErrorException('Error updating customer');
    }
  }

  async remove(id: string):Promise<Customer> {
    try {
      this.logger.debug('Deleting customer');
      const deletedCustomer = await this.customerModel.findByIdAndDelete(id);
      await this.openPayService.deleteCustomer(deletedCustomer.openPayId);
      return deletedCustomer;
    } catch (e) {
      this.logger.error(`Error looking customer by his id: ${e}`);
      throw new InternalServerErrorException('Error looking customer by his id');
    }
  }
}
