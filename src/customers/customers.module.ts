import { Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersResolver } from './customers.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { Customer, CustomerSchema } from './models/customer.model';
import { OpenpayModule } from 'src/openpay/openpay.module';

@Module({
  imports:[
    MongooseModule.forFeature([{ name: Customer.name, schema: CustomerSchema }]),
    OpenpayModule
  ],
  providers: [CustomersResolver, CustomersService],
  exports: [CustomersService]
})
export class CustomersModule {}
