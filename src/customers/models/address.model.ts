import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class Address{

    @Field((type) => String, {
        description: 'Customer address first line (street and ext. number)'
    })
    line1: string;

    @Field((type) => String,{
        description: 'Customer address second line (optional - delgation or municipality or town)',
        nullable: true
    })
    line2?: string;

    @Field((type) => String,{
        description: 'Customer address line 3 (optional - suburb or colony)',
        nullable: true
    })
    line3?: string;

    @Field((type) => String,{
        description: 'Customer postal code'
    })
    postal_code: string;

    @Field((type) => String,{
        description: 'Customer state address'
    })
    state: string;

    @Field((type) => String,{
        description: 'Customer city address'
    })
    city: string;

    @Field((type) => String, {
        description: 'Customer country code (ex: MX, US)',
        defaultValue: 'MX'
    })
    country_code: string;
}