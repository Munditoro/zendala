import { Field, ObjectType } from "@nestjs/graphql";
import { Prop, raw, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Charge } from "src/stores/models/charge.model";
import { Address } from "./address.model";

@ObjectType()
@Schema()
export class Customer{

    @Field(type => String, {description: 'Customer mongo id'})
    _id: MongooseSchema.Types.ObjectId;

    @Field(type => String, {description: 'Customer id assigned by openpay'})
    @Prop()
    openPayId:string;

    @Field((type) => String, {description: 'Customer name'})
    @Prop({required: true})
    name: string;

    @Field((type) => String, {
        description: 'Customer lastname (optional)',
        nullable: true
    })
    @Prop({nullable:true})
    last_name?: string;

    @Field((type) => String, {
        description: 'Customer email'
    })
    @Prop()
    email:string;

    @Field((type) => Boolean, {
        description: 'Check if the customer needs an account in openpay',
        nullable:true
    })
    @Prop({nullable:true})
    requires_account?: boolean;

    @Field(() => String, {
        description: 'Customer phone number (optional)',
        nullable: true
    })
    @Prop({nullable: true})
    phone_number?: string;

    @Field(type => Address, {
        description: 'Customer address object (optional)',
        nullable: true
    })
    @Prop({type:MongooseSchema.Types.Mixed, nullable: true})
    address?: Address;

    @Field((type) => [String], {
        description: 'Customer charges',
        nullable: true
    })
    @Prop({type: [MongooseSchema.Types.ObjectId], ref: Charge.name})
    charges: MongooseSchema.Types.ObjectId[];
}

export type CustomerDocument = Customer & Document;

export const CustomerSchema = SchemaFactory.createForClass(Customer);