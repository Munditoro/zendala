import { Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import axios from 'axios';
import { CreateChargeDTO } from './model/create-charge.dto';
import { CreateCustomerDTO } from './model/create-customer.dto';
import { UpdateCustomerDTO } from './model/update-customer.dto';
import Openpay = require('openpay');
import 'dotenv/config';

@Injectable()
export class OpenpayService {

    constructor(){}

    private readonly logger = new Logger(OpenpayService.name);
    private openpay = new Openpay(process.env.MERCHANTID, process.env.OPPRIVATEKEY);

    async createCustomer(createCustomerDTO: CreateCustomerDTO):Promise<Record<string, any>>{
        try {
            this.logger.debug('Creating new customer in openpay');
            return new Promise((resolve, reject) => {
                this.openpay.customers.create(createCustomerDTO, (error, body) => {
                    if(error) reject(error);
                    
                    this.logger.debug('Customer created');
                    resolve(body);
                });
            });
        } catch (e) {
            this.logger.error(`Error creating customer in openpay: ${e}`);
            throw new InternalServerErrorException('Error creating customer in openpay');
        }
    }

    async updateCustomer(custId:string, updateCustomerDTO: UpdateCustomerDTO):Promise<any>{
        try {
            this.logger.debug('Updating customer in openpay');
            return new Promise((resolve, reject) => {
                this.openpay.customers.update(custId, updateCustomerDTO, (err, body) => {
                    if(err) reject(err);
    
                    this.logger.debug('Customer updated');
                    resolve(body);
                });
            });
        } catch (e) {
            this.logger.debug(`Error updating customer in openpay: ${e}`);
            throw new InternalServerErrorException('Error updating customer in openpay'); 
        }
    }

    async readCustomer(id:string):Promise<any>{
        try {
            this.logger.debug('Looking customer in openpay');
            return new Promise((resolve, reject) => {
                this.openpay.customers.get(id, (err, body) => {
                    if(err) reject(err);
                    this.logger.debug('Customer found');
                    resolve(body);
                });
            });
        } catch (e) {
            this.logger.debug(`Error looking customer in openpay: ${e}`);
            throw new InternalServerErrorException('Error looking customer in openpay'); 
        }
    }

    async deleteCustomer(id:string):Promise<Record<string, any>>{
        try {
            this.logger.debug('Deleting customer in openpay');
            return new Promise((resolve, reject) => {
                this.openpay.customers.delete(id, (err, body) => {
                    if(err) reject(err);

                    this.logger.debug('Customer deleted in openpay');
                    resolve(body);
                });
            });
        } catch (e) {
            this.logger.debug(`Error deleting customer in openpay: ${e}`);
            throw new InternalServerErrorException('Error deleting customer in openpay'); 
        }
    }

    async createCharge(customerId:string, createChargeDTO: CreateChargeDTO):Promise<Record<string, any>>{
        try {
            this.logger.debug('Creating store charge in openpay');
            return new Promise((resolve, reject) => {
                this.openpay.customers.charges.create(customerId, createChargeDTO, (err, body) => {
                    if(err) reject(err);
                    
                    resolve(body);
                });
            });
        } catch (e) {
            this.logger.debug(`Error creating charge in openpay: ${e}`);
            throw new InternalServerErrorException('Error creating charge in openpay'); 
        }
    }

    async getStores(lat:number, lon: number, radio: number, amount: number):Promise<Record<string, any>>{
        try {
            this.logger.debug('Looking stores in parameters sended');
            return (await axios.get(`https://api.openpay.mx/stores?latitud=${lat}&longitud=${lon}&kilometers=${radio}&amount=${amount}`)).data;
        } catch (e) {
            this.logger.error(`Error looking stores: ${e}`);
            throw new NotFoundException('Error looking stores');
        }
    }
}
