import { IsDateString, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateChargeDTO{
    @IsNotEmpty()
    @IsString()
    method: string;

    @IsNotEmpty()
    @IsNumber()
    amount: number;

    @IsNotEmpty()
    @IsString()
    description: string;

    @IsOptional()
    @IsString()
    order_id?: string;

    @IsOptional()
    @IsDateString()
    due_date?: string;
}